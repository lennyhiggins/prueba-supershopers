const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/users/signin', (req, res) => {
    res.render('users/signin.hbs');
});

router.get('/users/signup', (req, res) => {
    res.render('users/signup.hbs');
});

router.post('/users/signup', async (req, res) => {
    const { name, email, password, confirm_password } = req.body;
    const errors = [];
    if (name.length <= 0){
        errors.push({text: "Please Insert your name"})
    }

    if (email.length <= 0){
        errors.push({text: "Please Insert your email"})
    }

    if (password.length <= 0){
        errors.push({text: "Please Insert your pasword"})
    }

    if (password != confirm_password){
        console.log('aqui');
        errors.push({text: "Password do not match"})
    }
    if (password.length < 4){
        errors.push({text: "Password must be at least 4 characters"})
    };

    if (errors.length > 0){
        //res.send('No Ok')
        res.render('users/signup.hbs', {errors, name, email, password, confirm_password})
    }else{
        console.log('aquiii');
        const emailUser = await User.findOne({email: email});
        if(emailUser){
            errors.push({text: "The Email is already in use"})
            res.render('users/signup.hbs', {errors, name, email, password, confirm_password})
           // req.flash('errors_msg', 'The Email is already in use');
           // res.redirect('/users/signup');
        }else{ 
            const newUser = new User({name, email, password});
            newUser.password = await newUser.encryptPassword(password);
            await newUser.save();
            req.flash('success_msg', 'You are registered');
            res.redirect('/users/signin');
        }
    }
});

module.exports = router;